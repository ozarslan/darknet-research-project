#!/usr/bin/python3

import os
import signal
import sys
import logging, logging.config

logging.config.dictConfig(dict(
    version = 1,
    formatters = {
        'f' : {
            'format': '%(asctime)s %(levelname)s %(name)s:%(lineno)d %(message)s'}
        },
    handlers = {
        'console' : {
            'class' : 'logging.StreamHandler',
            'formatter' : 'f',
            'level': 'DEBUG'},
        'logfile' : {
            'class' : 'logging.FileHandler',
            'filename' : __file__ + '.log',
            'formatter' : 'f'}
        },
    root = {
        'handlers' : ['console', 'logfile'],
        'level': 'DEBUG'}
))

import crawler

def handle_pdb(sig, frame):
    import pdb
    pdb.Pdb().set_trace(frame)

def main():
    cr = crawler.SimpleCrawler()
    if len(sys.argv) > 1:
        cr.continue_crawling()
    else:
        cr.start_crawling()

if __name__ == '__main__':
    logging.info('Starting crawling...')
    signal.signal(signal.SIGUSR1, handle_pdb)
    try:
        main()
    except Exception as e:
        logging.critical('Caught exception: %s', e)
        import pdb
        pdb.set_trace()
        raise
    finally:
        logging.info('Ending crawling...')
