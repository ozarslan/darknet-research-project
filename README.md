# A measurement study on DarkNet websites

## Project Summary

In this study, we collected various data from .onion Dark Net websites.

This research project is part of term project assignment for CS6301 Advanced
Topics in Internet Measurement and Network Security class in Fall 2017 semester
in the University of Texas at Dallas

## Requirements

* [python](http://www.python.org) >= 3.5.2
    * [requests](https://pypi.python.org/pypi/requests) >= 2.18.4
        * SOCKS support is required.
    * [beautifulsoup4](https://pypi.python.org/pypi/beautifulsoup4) >= 4.4.1
    * [pymongo](https://pypi.python.org/pypi/pymongo) >= 3.5.1
* [tor](https://www.torproject.org/) >= 0.3.1.7
* [mongodb](https://www.mongodb.com/) >= 3.4.10

## Project Advisor
Shuang Hao
<shao@utdallas.edu>

## Authors

Omer Ozarslan
<omer@utdallas.edu>

Zheng Yang
<Zheng.Yang@utdallas.edu>

