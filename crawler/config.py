from collections import OrderedDict

entry_points = [
    # Hidden wiki
    'http://zqktlwi4fecvo6ri.onion/wiki/index.php/Main_Page'
]

crawler_max_depth          = 5
crawler_max_pages_per_site = 500
crawler_timeout_secs       = 5.0
crawler_max_retries        = 2
crawler_max_page_len       = 16000000

database_name = 'meas2'

proxies = {
    'http' : 'socks5h://localhost:9050',
    'https' : 'socks5h://localhost:9050'
}

ua_strings = [
    'Mozilla/5.0 (Windows NT 6.1; rv:52.0) Gecko/20100101 Firefox/52.0'
]

default_headers = OrderedDict()
default_headers['User-Agent'] = ua_strings[0]
default_headers['Accept'] = '*/*'
default_headers['Accept-Language'] = 'en-US,en;q=0.5'
default_headers['Accept-Encoding'] = 'gzip, deflate'

