#!/usr/bin/python3

__version__ = '0.2.0'
__author__  = 'Omer Ozarslan'

import bs4
import datetime
import logging
import requests
import pdb
import re
import sys
import traceback

from bson.objectid  import ObjectId
from collections    import deque, namedtuple, OrderedDict
from pymongo        import MongoClient
from pymongo.errors import PyMongoError
from urllib.parse   import urljoin, urlparse

from . import config

logger = logging.getLogger(__name__)

QueueElement = namedtuple('QueueElement', ['referer', 'url', 'depth'])

class ExceptionWrapper(Exception):
    def __init__(self, *args):
        super().__init__(*args)

    def to_dict(self):
        return { 'message': self.args[0], 'args' : self.args[1:] }

class MaxRetriesReached(ExceptionWrapper):
    def __init__(self, max_retries):
        super().__init__('MaxRetriesReached', max_retries)

class TooManyRedirects(ExceptionWrapper):
    def __init__(self):
        super().__init__('TooManyRedirects')

class UnhandledException(ExceptionWrapper):
    def __init__(self, errstr):
        super().__init__('UnhandledException', errstr)

class TooLongPageContent(ExceptionWrapper):
    def __init__(self, page_len):
        super().__init__('TooLongPageContent', page_len)

class NotTextDocument(ExceptionWrapper):
    def __init__(self, content_type):
        super().__init__('NotTextDocument', content_type)

class CrawlingEntry:
    __slots__ = [
        '_id', 'url', 'redirected_url', 'referer', 'depth', 'timestamp',
        'elapsed', 'status_code', 'request_headers', 'response_headers',
        'response_content', 'darkweb_links', 'other_links', 'error']

    def __init__(self, url):
        self.url = url

    def to_dict(self):
        return {
            k: getattr(self, k) for k in self.__slots__ if hasattr(self, k)}

class SimpleCrawler:
    DARKWEB = re.compile(r'((https?://[^/]+?\.onion(?::\d+)?)(/[^#]*))')

    def __init__(self):
        self.db_client = MongoClient()
        self.queued    = set()
        self.queue     = deque()
        self.session   = requests.Session()
        self.session.headers = config.default_headers
        self.session.proxies = config.proxies
        self.db              = self.db_client[config.database_name]

    def start_crawling(self):
        self.queue.extend(
            [QueueElement(None, link, 0) for link in config.entry_points])
        self.queued.union(config.entry_points)
        self._consume_crawling_queue()

    def continue_crawling(self, continue_after = None):
        # TODO: Continue after given timestamp

        logger.info('Continuing crawling...')

        if self.db.crawled.count() == 0:
            return self.start_crawling()

        find = self.db.crawled.find

        # Find the very last element in very deep
        last_elm = find({}, {'referer' : 1, 'depth' : 1}
            ).sort([('depth', -1), ('_id', -1)]).limit(1)[0]

        last_ref_url = last_elm['referer']
        last_depth   = last_elm['depth']

        logger.info('Continuing from crawling links of %s in depth %d...',
            last_ref_url, last_depth - 1)

        # Set queued items to all urls
        self.queued.update(
            m['url'] for m in self.db.crawled.find({}, {'_id' : 0, 'url' : 1})
        )

        logger.info('%d links have been already crawled.', len(self.queued))

        # Find ID of last referer
        last_ref = self.db.crawled.find(
                {'$or' : [
                    { 'url' : last_ref_url },
                    { 'redirected_url' : last_ref_url}
                ], 'depth' : last_depth - 1},
            {'_id' : 1}
        ).sort([('_id', -1)]).limit(1)[0]

        # Fetch darkweb links (inclusively) after the last referer within the
        # same depth.
        last_queue_refs = self.db.crawled.find({
            '_id' : {'$gte' : last_ref['_id']},
            'depth' : last_depth - 1},
            {'_id' : 0, 'url' : 1, 'redirected_url' : 1, 'darkweb_links' : 1}
        )

        # Initialize queue
        for ref in last_queue_refs:
            if 'redirected_url' in ref:
                ref_url = ref['redirected_url']
            else:
                ref_url = ref['url']
            if 'darkweb_links' in ref:
                for link in ref['darkweb_links']:
                    anchorless_link = self.DARKWEB.match(link).group(0)
                    if anchorless_link not in self.queued:
                        self.queue.append(
                            QueueElement(ref_url, anchorless_link, last_depth))
                        self.queued.add(anchorless_link)

        logger.info('%d links have been queued.', len(self.queue))

        # Consume queue
        self._consume_crawling_queue()

    def _get_page(self, referer, url):
        logger.info('Getting URL: %s', url)
        req   = None
        tries = 0
        while req == None and tries < config.crawler_max_retries:
            try:
                req = self.session.get(
                    url, headers = {'Referer' : referer},
                    timeout = config.crawler_timeout_secs, stream = True)
            except requests.exceptions.TooManyRedirects as e:
                logger.debug(e)
                raise TooManyRedirects()
            except requests.exceptions.HTTPError as e:
                logger.debug(e)
            except requests.exceptions.Timeout as e:
                logger.debug(
                    'Connection to timed out after %f secs: %s',
                    config.crawler_timeout_secs, e)
            except requests.exceptions.RequestException as e:
                logger.debug('Unhandled error. Raising again: %s', e)
                raise
            tries += 1
        if not req:
            logger.debug(
                'Max retries have been reached for "%s".',
                url)
            raise MaxRetriesReached(config.crawler_max_retries)
        try:
            if not req.headers['Content-Type'].startswith('text/'):
                logger.debug(
                    'Skipping "%s" with content-type %s',
                    url, req.headers['Content-Type'])
                raise NotTextDocument(req.headers['Content-Type'])
        except KeyError:
            pass
        try:
            length = int(req.headers['Content-Length'])
            if  length > config.crawler_max_page_len:
                logger.debug(
                    'Skipping "%s" with content-length %d',
                    url, length)
                raise TooLongPageContent(length)
        except KeyError:
            if len(req.text) > config.crawler_max_page_len:
                raise TooLongPageContent(len(req.text))
        return req

    @staticmethod
    def _urljoin_raises(req_url, href, value):
        try:
            value[0] = urljoin(req_url, href)
            return False
        except Exception:
            return True

    def _extract_links(self, req):
        doc = bs4.BeautifulSoup(req.text, 'html.parser')
        value = [False]

        return (
            value[0] for a in doc.find_all('a', href=True)
                if not self._urljoin_raises(req.url, a['href'], value))

    def _consume_crawling_queue(self):
        while self.queue:
            referer, url, depth = self.queue.popleft()

            db_entry           = CrawlingEntry(url)
            db_entry.referer   = referer
            db_entry.depth     = depth
            db_entry.timestamp = datetime.datetime.utcnow()

            try:
                req = self._get_page(referer, url)
                self._handle_page(req, db_entry, depth)
            except ExceptionWrapper as e:
                db_entry.error = e.to_dict()
                logger.debug('Some error is being stored for url %s: %s',
                    url, e)
            except requests.exceptions.RequestException as e:
                db_entry.error = UnhandledException(str(e)).to_dict()
                logger.error('Unknown exception happened while trying to '
                    'get page %s', url, exc_info=1)
            try:
                result = self.db.crawled.insert_one(db_entry.to_dict())
                logger.debug('Inserted to database: %s', result)
            except PyMongoError as e:
                logger.error('Unknown exception happened while trying to '
                    'insert to database: %s\n%s', url, str(db_entry.to_dict()),
                    exc_info=1)

    def _handle_page(self, req, db_entry, depth):
        try:
            links = self._extract_links(req)
        except Exception:
            logger.error('Error happened during link extraction', exc_info=1)

        if len(req.history) > 0:
            db_entry.redirected_url = req.url
        db_entry.elapsed          = req.elapsed.total_seconds()
        db_entry.status_code      = req.status_code
        db_entry.request_headers  = req.request.headers
        db_entry.response_headers = req.headers
        db_entry.response_content = req.text
        db_entry.darkweb_links    = set()
        db_entry.other_links      = set()

        for link in links:
            logger.debug('Processing link "%s"', link)
            m = self.DARKWEB.match(link)
            if m:
                db_entry.darkweb_links.add(link)
                anchorless_link, hostname, path = m.groups()
                if (depth < config.crawler_max_depth
                        and anchorless_link not in self.queued):
                    self.queued.add(anchorless_link)
                    q_elm = QueueElement(
                        referer = req.url,
                        url = anchorless_link,
                        depth = depth+1)
                    self.queue.append(q_elm)
                    logger.debug('Queued: %s', q_elm)
                else:
                    logger.debug('Skipping darkweb link "%s"', link)
            else:
                db_entry.other_links.add(link)
                logger.debug('Skipping non-darkweb link "%s"', link)

        db_entry.darkweb_links = list(db_entry.darkweb_links)
        db_entry.other_links   = list(db_entry.other_links)

